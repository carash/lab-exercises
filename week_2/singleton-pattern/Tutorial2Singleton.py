# Mandatory 9: Edit IdnCurrRates class to a Singleton class
class IdnCurrRates (object):

        class __IdnCurrRates(object):

                def __init__(self,rates):
                        self.rates = rates

                def __str__(self):
                        return str(self.rates)

        __instance = None

        def __new__(cls, rates):
                if not IdnCurrRates.__instance:
                        IdnCurrRates.__instance = IdnCurrRates.__IdnCurrRates(rates)
                IdnCurrRates.__instance.rates = rates
                return IdnCurrRates.__instance

        def __getattr__(self, val):
                return getattr(self.__instance, val)

        def __setattr__(self, val):
                return setattr(self.__instance, val)
		
x = IdnCurrRates(10000)
print ('Rates x: ' + str(x))
y = IdnCurrRates(20000)
print ('Rates y: ' + str(y))
z = IdnCurrRates(30000)
print ('Rates z: ' + str(z))
print ('X and Y condition should be 30000')
print ('Rates x: ' + str(x))
print ('Rates y: ' + str(y))
