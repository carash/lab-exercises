class OnlyOne:

    class __OnlyOne:

        def __init__(self):
            self.val = None

        def __str__(self):
            return repr(self) + self.val

    __instance = None

    def __new__(cls):
        if not OnlyOne.__instance:
            OnlyOne.__instance = OnlyOne.__OnlyOne()
        return OnlyOne.__instance

    def __getattr__(self, name):
        return getattr(self.__instance, name)

    def __setattr__(self, name):
        return setattr(self.__instance, name)
