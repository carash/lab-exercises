#	Mandatory 3: Create a program so it implement Factory Method Pattern. There are a template class to use Factory Method Pattern but you are free to explore your idea in implementing Factory Method Pattern.

def main():
        n = input()
        while not n.isdigit() or int(n) < 0:
                n = input()
        board = Board3x3(int(n))
        print (board)

class AbstractBoard:
	
        def __init__(self, rows, columns):
                self.board = [[None for _ in range(columns)] for _ in range(rows)]
                self.populate_board()

        def populate_board(self):
                raise NotImplementedError()

        def __str__(self):
                squares = []
                for x, row in enumerate(self.board):
                        for y, column in enumerate(self.board):
                                squares.append(self.board[x][y])
                        squares.append("\n")
                return "".join(squares)

class Board3x3(AbstractBoard):
        def __init__(self, n):
                self.n = n
                super().__init__(n, n)

        def populate_board(self):
                for row in range(self.n):
                        for col in range(self.n):
                                if row & 1:
                                        if col&1:
                                                piece = TicTacToePiece.create_piece('v');
                                        else:
                                                piece = TicTacToePiece.create_piece('u');
                                else:
                                        if col&1:
                                                piece = TicTacToePiece.create_piece('o');
                                        else:
                                                piece = TicTacToePiece.create_piece('x');
                                self.board[row][col] = piece

class AbstractPiece():

        # Mandatory 3: Uncomment the codes below
        class Piece(str):
                __slots__ = ()

        @classmethod
        def create_piece(Class, name):
                raise NotImplementedError()

class TicTacToePiece(AbstractPiece):

        # Mandatory 3: Implement the Factory Method in here
        class Circle(AbstractPiece.Piece):
                __slots__ = ()
                
                def __new__ (Class):
                        return super().__new__(Class, "o")

        # Mandatory 3: Implement the Factory Method in here
        class Cross(AbstractPiece.Piece):
                __slots__ = ()
                
                def __new__ (Class):
                        return super().__new__(Class, "x")
                
        # Mandatory 3: Implement the Factory Method in here
        class Uvu(AbstractPiece.Piece):
                __slots__ = ()
                
                def __new__ (Class):
                        return super().__new__(Class, "u")

        # Mandatory 3: Implement the Factory Method in here
        class Vwe(AbstractPiece.Piece):
                __slots__ = ()
                
                def __new__ (Class):
                        return super().__new__(Class, "v")

        @classmethod
        def create_piece(Class, name):
                if name == 'o':
                        return Class.Circle()
                if name == 'x':
                        return Class.Cross()
                if name == 'u':
                        return Class.Uvu()
                if name == 'v':
                        return Class.Vwe()
                raise ValueError()

if __name__ == "__main__":
    main()
				
